//
//  ViewController.swift
//  MundoAnimal
//
//  Created by COTEMIG on 08/03/39 ERA1.
//

import UIKit
import Kingfisher
import Alamofire

struct Animal: Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {

    @IBOutlet weak var image_link: UIImageView!
    @IBOutlet weak var latim_name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnimal()
            }
            
            func getAnimal() {
                    AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
                        response in
                        if let animal = response.value {
                            self.image_link.kf.setImage(with: URL(string: animal.image_link))
                            self.latim_name.text = animal.latin_name
                            self.title = animal.name
                        }
                    }
                }
        }

